# Desafio Frontend - Bseller/Esmart - BIT SP

Para o teste, pedimos que seja entrega em até 5 dias, mas caso precise de mais tempo, nos avise que podemos negociar o prazo.

## Layout:

O layout do desafio a ser desenvolvido está nesse [link](https://www.dropbox.com/sh/m1idsr0fi2aek9v/AADxSSr0yBbg9etkPlAZUVeea?dl=0)

## Desafio:
Desenvolver a página do layout, está aberto quanto a quais tecnologias usar.

#### Obrigatório:
- Código HTML semântico
- Consumir o arquivo JSON para listar os produtos
- Design Responsivo

#### Desejável:
- Carousel de Produtos funcionando
- Interações que enriqueçam a navegação pelo layout

Crie um Fork desse repositório e nos envie um **pull request**.

Não esqueça de ensinar como instalamos e rodamos seu projeto em nosso ambiente. :sunglasses: